from django.conf.urls import patterns, include, url
import os.path
STATIC_ROOT=os.path.join(os.path.dirname(__file__),'templates/static')


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from loginap.views import *

urlpatterns = patterns('',
    # Examples:
    url(r'^$',log1),
    url(r'^log/',reg),
    url(r'^re/',home),
    url(r'^sin/',single),
    url(r'^se/',search),
    url(r'^display/',result),
    url(r'^logout/',logout),
    url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': STATIC_ROOT}),

    #url(r'^sin/',home),
    


   # url(r'^re/',reg),
    # url(r'^login/', include('login.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/',include(admin.site.urls)),
)
